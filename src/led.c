/* SPDX-License-Identifier: Apache-2.0
 * 
 * C_Demo2_Blink_Without_Delay
 * Copyright 2018 - 2021 The C_Demo2_Blink_Without_Delay authors
 * Copyright 2020 - 2021 ITlernpfad
 * 
 * ============================================================================
 * ============================================================================
 * This software contains software derived from portions of Lightweight millisecond 
 * tracking library (led.c) by Zak Kemble, with various modifications by 
 * ITlernpfad and other C_Demo2_Blink_Without_Delay authors, see 
 * C_Demo2_Blink_Without_Delay AUTHORS file.
 * 
 * Lightweight millisecond tracking library (http://blog.zakkemble.net/millisecond-tracking-library-for-avr/)
 * Copyright 2018 Zak Kemble
 * Dual-licensed under the GNU General Public License, Version 3.0 or the MIT license.
 */

/*
 * @package C_Demo2_Blink_Without_Delay
 * @file src/led.c
 * @authors 2018 - 2020 ITlernpfad and other C_Demo2_Blink_Without_Delay authors
 * (https://gitlab.com/ITlernpfad_MembersOnly/C_Demo2_Blink_Without_Delay/AUTHORS)
 * @copyright 2018 - 2021 The C_Demo2_Blink_Without_Delay authors
 * Copyright 2020 - 2021 ITlernpfad
 * Copyright 2018 Zak Kemble
 * @brief Part of C_Demo2_Blink_Without_Delay 
 * @details C_Demo2_Blink_Without_Delay blinks an LED in non-blocking pure C.  
 * language: C99
 * status: Beta
 * @version 1.0.2
 */

/*
 * Project: Lightweight millisecond tracking library
 * Author: Zak Kemble, contact@zakkemble.net
 * Copyright: (C) 2018 by Zak Kemble
 * License: GNU GPL v3 (see License_GPL-3.0.txt) or MIT (see License_MIT.txt)
 * Web: http://blog.zakkemble.net/millisecond-tracking-library-for-avr/
 */

// Blink 2 LEDs, built-in LED every 500ms and LED2 every 1000ms

#include <avr/io.h> /* https://www.nongnu.org/avr-libc/user-manual/group__avr__io.html . AVR device-specific IO definitions. */

#include <avr/interrupt.h> /* https://nongnu.org/avr-libc/user-manual/group__avr__interrupts.html . Interrrupts */

/* https://github.com/zkemble/millis . Provides functions millis_init and millis. Uses Clear Timer on Compare (CTC) mode of the selected timer (default: timer 2) */
#include "millis.h" 

/* https://gitlab.com/ITlernpfad_Public/uno_fastio . Arduino Uno Rev.3 device-specific IO definitions. */
#include "uno_fastio.h"

/* 
 * https://gitlab.com/ITlernpfad_Public/stdio_setup . Provides function UnartInit to redirect stdio and stderr streams to UART. 
 * On Arduino Uno Rev. 3, UART data is sent to USB.
 */
#include "stdio_setup.h"

/* State names */

/* states of finite state machine 0 */
#define state1off 0
#define state1on 1

/* states of finite state machine 1 */
#define state2off 0
#define state2on 1

/* State variables */
char state1 = state1off;
char state2 = state2off;  

#define onTime1 600U
#define onTime2 5000U

#define offTime1 500U
#define offTime2 1000U

int main() {

  /* Initialize millis library, https://github.com/zkemble/millis . Default timer: timer 2 */
  // #define MILLIS_TIMER MILLIS_TIMER0
  // #define MILLIS_TIMER MILLIS_TIMER1
  // #define MILLIS_TIMER MILLIS_TIMER2
  millis_init(); 

  /* Redirect stdio and stderr streams to UART. On Arduino Uno Rev. 3, UART data is sent to USB. */
  UartInit();

  /* Set Arduino pin 8 (PB0) and Arduino pin 13 (PB5) as outputs */
  DIO8_DDR |= (1U << DIO8_BIT);
  DIO13_DDR |= (1U << DIO13_BIT);

  /* Enable interrupts (https://nongnu.org/avr-libc/user-manual/group__avr__interrupts.html). Required by millis library (https://github.com/zkemble/millis) */
  sei();

  /* variables for keeping time LEDs last changed */
  millis_t lastChangeLed1Millis = 0U;
  millis_t lastChangeLed2Millis = 0U;

  for(;;) {

    /* Time now */
    millis_t now = millis();

    /* finite state machine 1 */
    if (state1 == state1on) {

      /* Has it been 600ms since last change for built-in LED1? */
      if (now - lastChangeLed1Millis >= onTime1) {

        /* Turn off built-in LED at digital I/O pin 13 of Arduino Uno Rev.3 */
        DIO13_PORT &= ~(1U << DIO13_BIT);

        /* Store time */
        lastChangeLed1Millis = now;
        state1 = state1off;
      }
    }

    if (state1 == state1off) {

      /* Has it been 500 ms since last change for built-in LED1? */
      if(now - lastChangeLed1Millis >= offTime1) {

        /* turn on built-in LED at digital I/O pin 13 of Arduino Uno Rev.3 */
        DIO13_PORT  |= (1U << DIO13_BIT);

        /* Store time */
        lastChangeLed1Millis = now;
        state1 = state1on;
      }
    }  

    /* finite state machine 2 */
    if(state2 == state2on) {

      /* Has it been 5000ms since last change for external LED2? */
      if(now - lastChangeLed2Millis >= onTime2) {

        /* Turn off built-in LED at digital I/O pin 8 of Arduino Uno Rev.3 */
        DIO8_PORT &= ~(1U << DIO8_BIT);
        printf("LED2 off");

        /* Store time */
        lastChangeLed2Millis = now;
        state2 = state2off;
      }
    }

    if (state2 == state2off) {

      /* Has it been 1000ms since last change for external LED2? */
      if (now - lastChangeLed2Millis >= offTime2) {

        /* Turn on external LED at digital I/O pin 8 of Arduino Uno Rev.3 */
        DIO8_PORT |= (1U << DIO8_BIT);
        printf("LED2 on");

        /* Store time */
        lastChangeLed2Millis = now;
        state2 = state2on;
      }
    }
  }
    
    return 0; // never reached
}
