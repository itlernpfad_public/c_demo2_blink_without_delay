# C_Demo2_Blink_Without_Delay

Blink LEDs in non-blocking C.

## Build instructions for C_Demo2_Blink_Without_Delay
1. Install the PlatformIO IDE (https://docs.platformio.org)
    1. `Install PlatformIO IDE for VSCode <https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode>`
    1. Install a `Git client <https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode>`
    1. Run Microsoft Visual Studio Code with the PlatformIO IDE. Left-click "File -> Preferences -> Settings -> Extensions". In section "Git", left-click on link to settings.json 
    1. in the file settings.json opened in step 1.3., verify if the entry for the key "git.path" specifies the correct path to the git executable. 
1. Install the Platform for the target
    1. Run Microsoft Visual Studio Code with the PlatformIO IDE. In the left VSCode Action toolbar, left-click "PlatformIO -> Quick Access -> Platforms -> Embedded". 
    1. Use the filter to search for "Atmel AVR". 
    1. Left-click on the platform "Atmel AVR -> Install".
    1. Wait until installation has succeeded and follow the instructions on screen. 
1. Clone C_Demo2_Blink_Witout_Delay
    1. Run Microsoft Visual Studio Code with the PlatformIO IDE. In the left toolbar, left-click "PlatformIO -> Quick Access -> Miscellaneous -> Clone Git Project". 
    1. Type "https://gitlab.com/itlernpfad_public/c_demo2_blink_without_delay" and press the enter-key. 
    1. Select a path for the local reposiory and confirm, then wait.
    1. In the notification at the bottom right, left-click "Add to Workspace".
1. Initiate the build process
    1. In VSCode, open the Explorer view: In the left VSCode Action toolbar, left-click "Explorer".
    1. In the Explorer panel of VSCode, left-click on the platformio.ini file at the root of the project "C_Demo2_Blink_Witout_Delay".
    1. Verify that platform.ini contains the correct content (for build target Arduino Uno Rev. 3, see below)
    1. In the PlatformIO toolbar at the bottom (<https://docs.platformio.org/en/latest//integration/ide/vscode.html#ide-vscode-toolbar>), left-click on the central "Project Environment Switcher" and select "Default (C_Demo2_Blink_Witout_Delay)". 
    1. In the PlatformIO toolbar at the bottom, left-click on the button "PlatformIO: Build" (alternatively, in the left VSCode Actio toolbar, click "PlatformIO -> Project tasks -> General -> Default -> Build All).
    1. In the PlatformIO toolbar at the bottom, left-click on the button "PlatformIO: Build" (alternatively, in the left VSCode Actio toolbar, click "PlatformIO -> Project tasks -> General -> Default -> Upload All).

plaformio.ini for build target Arduino Uno Rev.3:
```
[platformio]
default_envs = uno
include_dir = src  

[env]
lib_deps      = 
  https://gitlab.com/itlernpfad_public/stdio_setup.git

[common_uno]
platform = atmelavr
board = uno

upload_protocol = arduino

[env:uno]
extends = common_uno
build_type = release
build_flags = 
; compiler options for linking in an implementation of the printf function that supports floating point conversions. 
; https://www.nongnu.org/avr-libc/user-manual/group__avr__stdio.html
  -Wl,-u,vfprintf
  -lprintf_flt
  -lm
```

## License Information

C_Demo2_Blink_Without_Delay
Copyright 2018 - 2021 The C_Demo2_Blink_Without_Delay authors
Copyright 2020 ITlernpfad

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This software contains software derived from portions of Lightweight millisecond tracking library by Zak Kemble, with various modifications by ITlernpfad and Demo2_Blink_Without_Delay authors, see C_Demo2_Blink_Without_Delay AUTHORS file.

Lightweight milliracking library (http://blog.zakkemble.netmillisecond-trackary-for-avr)
Copyright 2018 Zak Kemble
Dual-licensed under the GNU Geral Public License, Version 3.0, or the MIT License.

This software includes uno_fastio (https://gitlab.com/itlernpfad_public/uno_fastio/)

uno_fastio (https://itlernpfad_public/uno_fastio)
Copyright 2003 - 2021 The uno_fastio Authors (https://gitlab.com/itlernpfad_public/uno_fastio/-/blob/master/AUTHORS)
Copyright 2021 ITlernpfad
Copyright 2011 Kliment
Copyright 2010 - 2011 Triffid_Hunter
This work is licensed under the GNU Lesser General Public License, Version 3 or any later version.
For details, see file LICENSES/LGPL-3.0.txt
